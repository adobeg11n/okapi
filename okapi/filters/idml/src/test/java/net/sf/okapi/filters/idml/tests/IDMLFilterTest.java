/*
 * =============================================================================
 *   Copyright (C) 2010-2017 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml.tests;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.filters.idml.IDMLFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class IDMLFilterTest {

    private IDMLFilter filter;
    private FileLocation root;
    private LocaleId locEN = LocaleId.fromString("en");

    @Before
    public void setUp() {
        filter = new IDMLFilter();
        root = FileLocation.fromClass(this.getClass());
    }

    @Test
    public void testDefaultInfo() {
        assertThat(filter.getParameters(), is(notNullValue()));
        assertThat(filter.getName(), is(notNullValue()));

        List<FilterConfiguration> filterConfigurations = filter.getConfigurations();
        assertThat(filterConfigurations, is(notNullValue()));
        assertThat(filterConfigurations.size(), is(not(0)));
    }

    @Test
    public void testSimpleEntry() {
        List<ITextUnit> textUnits = getTextUnits("/helloworld-1.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "Hello World!");
    }

    @Test
    public void testSimpleEntry2() {
        List<ITextUnit> textUnits = getTextUnits("/Test00.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "\uE101\uE110Hello \uE101\uE111World!\uE102\uE112\uE102\uE113Hello again \uE101\uE114World!\uE102\uE115");
        assertThat(textUnits.get(0).toString(), is(equalTo("<content-1>Hello <content-2>World!</content-2></content-1>Hello again <content-3>World!</content-3>")));
        assertThat(TextFragment.getText(textUnits.get(0).getSource().getCodedText()), is(equalTo("Hello World!Hello again World!")));
    }

    @Test
    public void testWhitespaces() {
        List<ITextUnit> textUnits = getTextUnits("/tabsAndWhitespaces.idml");

        assertThat(textUnits, is(notNullValue()));
        assertThat(textUnits.size(), is(14));

        assertThat(textUnits.get(0).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "Hello World.");

        assertThat(textUnits.get(1).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 1, "Hello\tWorld with a Tab.");

        assertThat(textUnits.get(2).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 2, "Hello \tWorld with a Tab and a white space.");

        assertThat(textUnits.get(3).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 3, " Hello World\t.");

        assertThat(textUnits.get(4).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 4, "\uE101\uE110Hello World.\uE102\uE111");

        assertThat(textUnits.get(5).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 5, "Hello      World.\uE103\uE110");

        assertThat(textUnits.get(6).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 6, "\uE103\uE110");

        assertThat(textUnits.get(7).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 7, " Hello World\t.");

        assertThat(textUnits.get(8).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 8, "HelloWorldwithout.");

        assertThat(textUnits.get(9).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 9, "Hello \tWorld with a Tab and a white space.");

        assertThat(textUnits.get(10).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 10, "m-space here.");

        assertThat(textUnits.get(11).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 11, "n-space here.");

        assertThat(textUnits.get(12).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 12, "another m-space\uE103\uE110here.");

        assertThat(textUnits.get(13).preserveWhitespaces(), is(true));
        assertThatTextUnitsContainTextUnitWithText(textUnits, 13, "another one here.");
    }

    @Test
    public void testNewline() {
        List<ITextUnit> textUnits = getTextUnits("/newline.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "\uE101\uE11032\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 1, "\uE101\uE110Hello World\uE102\uE111");
    }

    @Test
    public void testStartDocument() {
        assertTrue("Problem in StartDocument", FilterTestDriver.testStartDocument(filter,
                new InputDocument(root.in("/Test01.idml").toString(), null),
                "UTF-8", locEN, locEN));
    }

    @Test
    public void testObjectsWithoutPathPointsAndText() {
        List<ITextUnit> textUnits = getTextUnits("/618-objects-without-path-points-and-text.idml");

        assertThat(textUnits.size(), is(0));
    }

    @Test
    public void testAnchoredFrameWithoutPathPoints() {
        List<ITextUnit> textUnits = getTextUnits("/618-anchored-frame-without-path-points.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 4, "Anchored");
    }

    @Test
    public void testDocumentWithoutPathPoints() {
        List<ITextUnit> textUnits = getTextUnits("/618-MBE3.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "\uE101\uE110Fashion Industry In Colombia\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 1, "\uE101\uE110\uE103\uE111\uE102\uE112");
    }

    @Test
    public void testSkipDiscretionaryHyphens() throws Exception {
        filter.getParameters().setSkipDiscretionaryHyphens(true);

        List<ITextUnit> textUnits = getTextUnits("/Bindestrich.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "Ich bin ein bedingter Bindestrich.");
    }

    @Test
    public void testChangeTracking() {
        List<ITextUnit> textUnits = getTextUnits("/08-conditional-text-and-tracked-changes.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "\uE101\uE110Conditional Text Sample\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 1, "\uE101\uE110New text.\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 2, "\uE101\uE110This simple document demonstrates controlling conditional text visibility.\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 3, "\uE101\uE110Print Only\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 4, "\uE101\uE110\tThis text is print only!\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 5, "\uE101\uE110Web Only\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 6, "\uE101\uE110\tThis text is Web only!\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 7, "\uE101\uE110BREAKING NEWS!\uE102\uE111");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 8, "\uE101\uE110Print and Breaking News\uE102\uE111");

        textUnits = getTextUnits("/change-tracking-3.idml");

        assertThatTextUnitsContainTextUnitWithText(textUnits, 0, "Text 1 ");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 1, "Text 2");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 2, "Text 3");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 3, "Text 4");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 4, "Text 5 \uE101\uE110\uE103\uE111\uE102\uE112Text 6");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 5, "Text 7");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 6, "Text 10");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 7, "Text 11");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 8, "Text 13");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 9, "Text 14");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 10, "Text 15");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 11, "Text 16");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 12, "Text 17");
        assertThatTextUnitsContainTextUnitWithText(textUnits, 13, "Text 18");
    }

    private List<ITextUnit> getTextUnits(String testFileName) {

        return FilterTestDriver.filterTextUnits(
                FilterTestDriver.getEvents(
                        filter,
                        new RawDocument(root.in(testFileName).asUri(), "UTF-8", locEN),
                        null
                )
        );
    }

    private void assertThatTextUnitsContainTextUnitWithText(List<ITextUnit> textUnits, int index, String expectedText) {
        ITextUnit textUnit = textUnits.get(index);

        assertThat(textUnit, is(notNullValue()));
        assertThat(textUnit.getSource().getFirstContent().getCodedText(), is(equalTo(expectedText)));
    }

    @DataProvider
    public static Object[][] testDoubleExtractionProvider() {
        return new Object[][]{
                {"Test00.idml", "okf_idml@ExtractAll.fprm"},
                {"Test01.idml", "okf_idml@ExtractAll.fprm"},
                {"Test02.idml", "okf_idml@ExtractAll.fprm"},
                {"Test03.idml", "okf_idml@ExtractAll.fprm"},

                {"helloworld-1.idml", "okf_idml@ExtractAll.fprm"},
                {"ConditionalText.idml", "okf_idml@ExtractAll.fprm"},

                {"testWithSpecialChars.idml", "okf_idml@ExtractAll.fprm"},

                {"TextPathTest01.idml", "okf_idml@ExtractAll.fprm"},
                {"TextPathTest02.idml", "okf_idml@ExtractAll.fprm"},
                {"TextPathTest03.idml", "okf_idml@ExtractAll.fprm"},
                {"TextPathTest04.idml", "okf_idml@ExtractAll.fprm"},

                {"idmltest.idml", "okf_idml@ExtractAll.fprm"},
                {"idmltest.idml", null},

                {"01-pages-with-text-frames.idml", null},
                {"01-pages-with-text-frames-2.idml", null},
                {"01-pages-with-text-frames-3.idml", null},
                {"01-pages-with-text-frames-4.idml", null},
                {"01-pages-with-text-frames-5.idml", null},
                {"01-pages-with-text-frames-6.idml", null},

                {"02-island-spread-and-threaded-text-frames.idml", null},
                {"03-hyperlink-and-table-content.idml", null},
                {"04-complex-formatting.idml", null},
                {"05-complex-ordering.idml", null},

                {"06-hello-world-12.idml", null},
                {"06-hello-world-13.idml", null},
                {"06-hello-world-14.idml", null},

                {"07-paragraph-breaks.idml", null},

                {"08-conditional-text-and-tracked-changes.idml", null},
                {"change-tracking-3.idml"},
                {"08-direct-story-content.idml", null},

                {"09-footnotes.idml", null},
                {"10-tables.idml", null},

                {"11-xml-structures.idml", "okf_idml@ExtractAll.fprm"},
                {"11-xml-structures.idml", null},

                {"618-objects-without-path-points-and-text.idml", null},
                {"618-anchored-frame-without-path-points.idml", null},
                {"618-MBE3.idml", null},
                {"/Bindestrich.idml", null},
        };
    }

    @Test
    @UseDataProvider("testDoubleExtractionProvider")
    public void testDoubleExtraction(String inputDocumentName, String parametersFileName) {
        List<InputDocument> list = new ArrayList<>();
        list.add(new InputDocument(root.in("/" + inputDocumentName).toString(), parametersFileName));


        RoundTripComparison rtc = new RoundTripComparison(false); // Do not compare skeleton
        assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN, "output"));
    }
}
