package net.sf.okapi.filters.xmlstream.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;

public class AdobePOTest {

	private XmlStreamFilter xmlStreamFilter;	
	private String root;
	private LocaleId locEN = LocaleId.ENGLISH;
	private FilterConfigurationMapper fcMapper;
	
	@Before
	public void setUp() throws Exception {		       
		xmlStreamFilter = new XmlStreamFilter();			
		URL url = AdobePOTest.class.getResource("/adobepo.xml");				
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
		
		fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
		fcMapper.addConfigurations("net.sf.okapi.filters.xmlstream.XmlStreamFilter");
		fcMapper.setCustomConfigurationsDirectory(root);
        fcMapper.updateCustomConfigurations();
        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
	}
		
	@Test
	public void test() throws URISyntaxException, IOException {
		xmlStreamFilter.setParametersFromURL(XmlStreamFilter.class.getResource("/okf_xmlstream@adobepo.fprm"));
		RoundTripComparison rtc = new RoundTripComparison();
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		list.add(new InputDocument(root + "adobepo.xml", "okf_xmlstream@adobepo.fprm"));
		assertTrue(executeCompare(xmlStreamFilter, list, "UTF-8", locEN, locEN, "out"));
	}
	
	public boolean executeCompare (IFilter filter,
			List<InputDocument> inputDocs,
			String defaultEncoding,
			LocaleId srcLoc,
			LocaleId trgLoc,
			String dirSuffix) throws IOException
		{
			if (Util.isEmpty(dirSuffix)) throw new InvalidParameterException("dirSuffix cannot be empty - an attempt to override the source file will be rejected and source file will be compared with itself returning always true");
			//return executeCompare(filter, inputDocs, defaultEncoding, srcLoc, trgLoc, dirSuffix, (IPipelineStep[]) null);

			for (InputDocument doc : inputDocs) {
				// Load parameters if needed, !!! no reset is called for parameters
				if (doc.paramFile != null && !doc.paramFile.equals("")) {
					String root = Util.getDirectoryName(doc.path);
					IParameters params = filter.getParameters();
					if (params != null) {
						params.load(Util.toURL(root + File.separator + doc.paramFile), false);
					}
				}
				// Execute the first extraction and the re-writing
				String outPath = executeFirstExtractionToFile(filter, doc, dirSuffix, defaultEncoding,
						srcLoc, trgLoc, (IPipelineStep[]) null);
				// Compare the files
				String inFile = readFileToString(doc.path);
				String outFile = readFileToString(outPath);
				System.out.println("inFile="+inFile);
				System.out.println("outFile="+outFile);
				assertEquals(inFile, outFile);
			}
			return true;
		}
	
	private String readFileToString(String path) throws IOException {
		FileInputStream fis = new FileInputStream(path);
		BufferedReader frdr = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
		StringBuilder strBldr = new StringBuilder();
		try {
			
			String str;
	        while ((str = frdr.readLine()) != null) {
	        	strBldr.append(str).append("\n");
	        }
	        
	        return strBldr.toString();
		} finally {
			if(frdr!=null) frdr.close();
			if(fis!=null) fis.close();
		}
	}

	private String executeFirstExtractionToFile(IFilter filter, InputDocument doc, String outputDir, String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, IPipelineStep... steps) {
		String outPath = null;
		IFilterWriter writer = null;
		try {
			// Open the input
			filter.open(new RawDocument(Util.toURI(doc.path), defaultEncoding, srcLoc,
					trgLoc));

			// Prepare the output
			// Create the filter-writer for the provided filter
			writer = filter.createFilterWriter();
			writer.setOptions(trgLoc, "UTF-8");
			outPath = Util.getDirectoryName(doc.path);
			if ( Util.isEmpty(outputDir) ) {
				outPath += (File.separator + Util.getFilename(doc.path, true));
			}
			else {
				outPath += (File.separator + outputDir + File.separator + Util.getFilename(doc.path, true));
			}
			writer.setOutput(Util.fixPath(outPath));
			
			// Process the document
			Event event;
			while (filter.hasNext()) {
				event = filter.next();
				
				if (steps != null) {
					for (IPipelineStep step : steps) {
						event = step.handleEvent(event);						
					}
				}					
				writer.handleEvent(event);
			}
		} finally {
			if (filter != null)
				filter.close();
			if (writer != null)
				writer.close();
		}
		return outPath;
	}

}