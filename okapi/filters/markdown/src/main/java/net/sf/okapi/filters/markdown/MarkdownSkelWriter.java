package net.sf.okapi.filters.markdown;

import java.io.StringWriter;
import java.util.Collections;
import java.util.Map;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartSubfilter;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.filters.markdown.parser.MarkdownTokenType;

public class MarkdownSkelWriter extends GenericSkeletonWriter {

	private Parameters params;

	public MarkdownSkelWriter(Parameters params) {
		super();
		this.params = params;
	}
	
	@Override 
	public String processStartSubfilter(StartSubfilter resource) {
		String str = super.processStartSubfilter(resource);
		IEncoder parentEncoder = resource.getParentEncoder();
		if(parentEncoder instanceof EncoderManager) {
			EncoderManager parentEM = (EncoderManager) parentEncoder;
			parentEM.updateEncoder(MimeTypeMapper.MARKDOWN_MIME_TYPE);
		}
		return str;
	}
	
	@Override
	protected String getContent(ITextUnit tu, LocaleId locToUse, EncoderContext context) {
		if(params.getEncodeHTMLEntitiesOnWrite() && !isYamlHeaderVal(tu)) {
			tu.setMimeType(MimeTypeMapper.HTML_MIME_TYPE.toString());
		}
		String superContent = super.getContent(tu, locToUse, context);
		
		if(isYamlHeaderVal(tu)) {
			superContent = superContent.replaceAll("\\n", " ").replaceAll("\\r", "");
			superContent = convertToYamlStringLiteral(superContent);
		} else {
			superContent = superContent.replaceAll("\u200B", "&#x200B;");
			
			if(isTableCellText(tu)) {
				superContent = superContent.replaceAll("\\n", " ").replaceAll("\\r", "");
			}
		
			superContent = superContent.replaceAll("\\[!UICONTROL\\s*\\]", "");
		}
		
		return superContent;
	}

	private String convertToYamlStringLiteral(String superContent) {
		DumperOptions dumperOptions = new DumperOptions();
		dumperOptions.setSplitLines(false);
		Yaml yaml = new Yaml(dumperOptions);
		Map<String, String> data = Collections.singletonMap("dummy", superContent);
		StringWriter writer = new StringWriter();
		yaml.dump(data, writer);
		StringBuffer output = writer.getBuffer();
	    output.delete(0, "{dummy: ".length());
	    output.setLength(output.length()-"}\n".length());
	    return output.toString();
	}
	
	private boolean isTableCellText(ITextUnit tu) {
		return tu.getType()!=null && tu.getType().equals(MarkdownTokenType.TABLECELL_TEXT.name());
	}
	
	private boolean isYamlHeaderVal(ITextUnit tu) {
		return tu.getType()!=null && tu.getType().equals(MarkdownTokenType.YAML_METADATA_HEADER_VALUE.name());
	}
}
