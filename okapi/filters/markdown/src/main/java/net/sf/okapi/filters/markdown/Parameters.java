/*===========================================================================
  Copyright (C) 2017-2018 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.markdown;

import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.encoder.HtmlEncoder;
import net.sf.okapi.common.filters.InlineCodeFinder;

public class Parameters extends StringParameters {
    private static final String HTML_SUBFILTER_CONFIG_PATH = "htmlSubfilter";
    private static final String USECODEFINDER = "useCodeFinder";
    private static final String USESPECIALCODEFINDER = "useSpecialCodeFinder";
    private static final String CODEFINDERRULES = "codeFinderRules";
    private static final String TRANSLATE_URLS = "translateUrls";
    private static final String TRANSLATE_CODE_BLOCKS = "translateCodeBlocks";
    private static final String TRANSLATE_HEADER_METADATA = "translateHeaderMetadata";
    private static final String TRANSLATE_IMAGE_ALTTEXT = "translateImageAltText";
    private static final String URL_TO_TRANSLATE_PATTERN = "urlToTranslatePattern";
    private static final String URL_IN_SEPARATE_TU = "urlInSeparateTU";
    private static final String NON_TRANSLATE_BLOCKS ="nonTranslateBlocks";
    private static final String TRANSLATE_HEADER_KEYS_REGEX = "translateHeaderKeysRegex";
    private static final String NON_TRANSLATE_HEADER_KEYS_REGEX = "nonTranslateHeaderKeysRegex";
	private static final String DECODE_HTML_ENTITIES = "decodeHtmlEntities";
	private static final String ENCODE_HTML_ENTITIES_ON_WRITE = "encodeHtmlEntitiesOnWrite";
	private static final String ENABLE_NESTED_BLOCK_QUOTE_FIX = "enableNestedBlockQuoteFix";

    private InlineCodeFinder codeFinder; // Initialized in reset()

    public Parameters() {
	reset();
    }
    
    /**
     * The configuration that the HTML subfilter uses, if set.
     * @return The configuration file path, or null if Markdown filter's default HTML configuration is used.
     */
    public String getHtmlSubfilter() {
        return getString(HTML_SUBFILTER_CONFIG_PATH);
    }

    /**
     * Uses the user-supplied HTML sub-filter configuration rather than Markdown's default.
     * @param htmlSubfilter The path of the configuration yml file which typically has the .fprm suffix.
     */
    public void setHtmlSubfilter(String htmlSubfilter) {
        setString(HTML_SUBFILTER_CONFIG_PATH, htmlSubfilter);
    }
 
	public boolean getEscapeGT () {
		return getBoolean(HtmlEncoder.ESCAPEGT);
	}
	
	public void setEscapeGT (boolean escapeGT) {
		setBoolean(HtmlEncoder.ESCAPEGT, escapeGT);
	}
	
	public boolean getUrlInSeparateTU () {
		return getBoolean(URL_IN_SEPARATE_TU);
	}

	public void setUrlInSeparateTU (boolean urlInSeparateTU) {
		setBoolean(URL_IN_SEPARATE_TU, urlInSeparateTU);
	}

    public boolean getUseCodeFinder() {
        return getBoolean(USECODEFINDER);
    }
    
    public void setUseCodeFinder(boolean useCodeFinder) {
        setBoolean(USECODEFINDER, useCodeFinder);
    }

    public boolean getTranslateUrls() {
        return getBoolean(TRANSLATE_URLS);
    }

    public void setTranslateUrls(boolean translateUrls) {
        setBoolean(TRANSLATE_URLS, translateUrls);
    }

    public String getUrlToTranslatePattern() {
    	return getString(URL_TO_TRANSLATE_PATTERN);
    }

    public void setUrlToTranslatePattern(String urlToTranslatePattern) {
    	setString(URL_TO_TRANSLATE_PATTERN, urlToTranslatePattern);
    }
    
    public boolean getTranslateCodeBlocks() {
        return getBoolean(TRANSLATE_CODE_BLOCKS);
    }

    public void setTranslateCodeBlocks(boolean translateCodeBlocks) {
        setBoolean(TRANSLATE_CODE_BLOCKS, translateCodeBlocks);
    }

    public boolean getTranslateHeaderMetadata() {
        return getBoolean(TRANSLATE_HEADER_METADATA);
    }

    public void setTranslateHeaderMetadata(boolean translateHeaderMetadata) {
        setBoolean(TRANSLATE_HEADER_METADATA, translateHeaderMetadata);
    }

    public boolean getTranslateImageAltText() {
        return getBoolean(TRANSLATE_IMAGE_ALTTEXT);
    }

    public void setTranslateImageAltText(boolean translateImageAltText) {
        setBoolean(TRANSLATE_IMAGE_ALTTEXT, translateImageAltText);
    }

    public InlineCodeFinder getCodeFinder() {
        return codeFinder;
    }
    
    public String getNonTranslateBlocks() {
        return getString(NON_TRANSLATE_BLOCKS);
    }

    public void setNonTranslateBlocks(String nonTranslatableBlocks) {
        setString(NON_TRANSLATE_BLOCKS, nonTranslatableBlocks);
    }
    
    public String getNonTranslateHeaderKeysPattern() {
        return getString(NON_TRANSLATE_HEADER_KEYS_REGEX);
    }

    public void setNonTranslateHeaderKeysPattern(String nonTranslatableHeaderKeysRegex) {
        setString(NON_TRANSLATE_HEADER_KEYS_REGEX, nonTranslatableHeaderKeysRegex);
    }
    
    public String getTranslateHeaderKeysPattern() {
        return getString(TRANSLATE_HEADER_KEYS_REGEX);
    }

    public void setTranslateHeaderKeysPattern(String translatableHeaderKeysRegex) {
        setString(TRANSLATE_HEADER_KEYS_REGEX, translatableHeaderKeysRegex);
    }

    public void setDecodeHTMLEntities(boolean decodeHTMLEntities) {
        setBoolean(DECODE_HTML_ENTITIES, decodeHTMLEntities);
    }

    public boolean getDecodeHTMLEntities() {
        return getBoolean(DECODE_HTML_ENTITIES);
    }
    
    public void setEncodeHTMLEntitiesOnWrite(boolean encodeHTMLEntitiesOnWrite) {
        setBoolean(ENCODE_HTML_ENTITIES_ON_WRITE, encodeHTMLEntitiesOnWrite);
    }

    public boolean getEncodeHTMLEntitiesOnWrite() {
        return getBoolean(ENCODE_HTML_ENTITIES_ON_WRITE);
    }
    
    public void setEnableNestedBlockQuoteFix(boolean enableNestedBlockQuoteFix) {
        setBoolean(ENABLE_NESTED_BLOCK_QUOTE_FIX, enableNestedBlockQuoteFix);
    }

    public boolean getEnableNestedBlockQuoteFix() {
        return getBoolean(ENABLE_NESTED_BLOCK_QUOTE_FIX);
    }

    @Override
    public void reset() {
        super.reset();
        setHtmlSubfilter(null);
        setUseCodeFinder(false);
        setUseSpecialCodeFinder(false);
        setTranslateUrls(false);
        setUrlToTranslatePattern(".+");
        setTranslateCodeBlocks(true);
        setTranslateHeaderMetadata(false);
        setTranslateHeaderKeysPattern(null);
        setNonTranslateHeaderKeysPattern(null);
        setTranslateImageAltText(true);
        setDecodeHTMLEntities(false);
        setEncodeHTMLEntitiesOnWrite(false);
        setEnableNestedBlockQuoteFix(false);

        codeFinder = new InlineCodeFinder();
        codeFinder.setSample("{{#test}} handle bar test {{/test}}\n{{stand-alone handle bar}}\n");
        codeFinder.setUseAllRulesWhenTesting(true);
        codeFinder.addRule("\\{\\{[^}]+\\}\\}");
    }

    public void fromString (String data) {
        super.fromString(data);
        codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
    }

    @Override
    public String toString () {
        buffer.setGroup(CODEFINDERRULES, codeFinder.toString());
        return super.toString();
    }

	public boolean getUseSpecialCodeFinder() {
        return getBoolean(USESPECIALCODEFINDER);
    }
    
    public void setUseSpecialCodeFinder(boolean useSpecialCodeFinder) {
        setBoolean(USESPECIALCODEFINDER, useSpecialCodeFinder);
    }

}
