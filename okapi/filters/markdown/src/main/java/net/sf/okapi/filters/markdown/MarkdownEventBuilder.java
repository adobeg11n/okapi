package net.sf.okapi.filters.markdown;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;

public class MarkdownEventBuilder extends EventBuilder {
    private InlineCodeFinder codeFinder;
	private boolean useSpecialCodeFinder = false;

    public MarkdownEventBuilder(String rootId, IFilter subFilter) {
        super(rootId, subFilter);
        codeFinder = null;
        setMimeType(MimeTypeMapper.MARKDOWN_MIME_TYPE);
    }

    @Override
    protected ITextUnit postProcessTextUnit(ITextUnit textUnit) {
        TextFragment text = textUnit.getSource().getFirstContent();
        if (codeFinder != null) {
            codeFinder.process(text);
        }
        if (useSpecialCodeFinder) {
	        //special  handling to convert UICONTROL to placeholder
	        specialPlaceholderFinder(text);
        }
        return textUnit;
    }

    public void setCodeFinder(InlineCodeFinder codeFinder) {
        this.codeFinder = codeFinder;
    }
    
	/**
	 * Applies special rule to convert [!UICONTROL SOme Text <placeholder for DNL> some other text]
	 * to placeholders
	 * @param fragment The fragment where to apply the rules.
	 */
	private void specialPlaceholderFinder (TextFragment fragment) {
		Pattern pattern = Pattern.compile("(\\[!UICONTROL )[^\\]]*[\\uE101\\uE102\\uE103\\uE104]?[^\\]]*(])");
		String tmp = fragment.getCodedText();
		Matcher m = pattern.matcher(tmp);
		int start = 0;
		int diff = 0;
		while ( m.find(start) ) {
			// set the code displayText as the matched regex subsequence
			diff += fragment.changeToCode(m.start(1)+diff, m.end(1)+diff,
				TagType.PLACEHOLDER, InlineCodeFinder.TAGTYPE, true);
			diff += fragment.changeToCode(m.start(2)+diff, m.end(2)+diff,
					TagType.PLACEHOLDER, InlineCodeFinder.TAGTYPE, true);
			start = m.end();
			// Check the case where the last match was at the end
			// which makes the next start invalid for find().
			if ( start >= tmp.length() ) break;
		}
	}

	public void setUseSpecialCodeFinder(boolean useSpecialCodeFinder) {
		this.useSpecialCodeFinder = useSpecialCodeFinder;
	}
}
