package net.sf.okapi.common.filters;

import static org.junit.Assert.*;

import org.junit.Test;

import net.sf.okapi.common.resource.TextFragment;

public class InlineCodeFinderTest {

	@Test
	public void test() {
		InlineCodeFinder codeFinder = new InlineCodeFinder();
		codeFinder.addRule("#\\{?((if)|(elseif)|(foreach)|(set)|(include)|(parse)|(evaluate))\\}?\\s*\\(.*?\\)");
		codeFinder.compile();
		TextFragment fragment = new TextFragment("#if(!${named_user_licenses.isEmpty()})PLAN | NAMED-USER LICENSES\r\n" + 
				"#foreach(${license} in ${named_user_licenses})$!esc.html($!{license.plan}) | $!esc.html($!{license.quantity})#end#end\r\n" + 
				"\r\n" + 
				"Thanks and enjoy,\r\n" + 
				"The Adobe Scan Team");
		codeFinder.process(fragment);
		codeFinder.balanceCodeParenthesis(fragment);
		System.out.println(fragment.getCodes());
		assertArrayEquals(new String[] { "#if(!${named_user_licenses.isEmpty()})", "#foreach(${license} in ${named_user_licenses})" }, 
				fragment.getCodes().stream().map(c -> c.toString()).toArray(String[]::new));
	}

}
